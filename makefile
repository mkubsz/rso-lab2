all: server client

server: server.o
	cc -o $@ $< -lm

server.o: server.c
	cc -c -o $@ $< -lm

client: client.o
	cc -o $@ $< -lm

client.o: client.c
	cc -c -o $@ $< -lm

.PHONY : clean

clean : 
	-rm server.o client.o server client