#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <string.h>

void roots(double a, double b, double c, int socket);
void date(int socket);
bool isLittleEndian();
void swapbytes(double*_object, size_t _size);

int main(){
	
	int clientSocket, result;
	socklen_t sock_len;
	struct sockaddr_in address;		
	clientSocket = socket (AF_INET, SOCK_STREAM,0);
	
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr("127.0.0.1");
	address.sin_port = htons(7500);
	sock_len = sizeof(address);
	
	if((connect(clientSocket, (struct sockaddr *) &address, sock_len)) == -1){
		perror ("Netclient");
		exit(1);
	}
	if(isLittleEndian)
		
	//roots(2, 5, 2, clientSocket);
	
	date(clientSocket);
	
	close(clientSocket);
	
	exit(0);
}

bool isLittleEndian(){
	short int number = 0x1;
	char *numPtr = (char*)&number;
	return (numPtr[0] == 1);
}

void swapbytes(double*_object, size_t _size){
	unsigned char *start, *end;
	
	if(isLittleEndian()){
		for(start = (unsigned char *)_object, end = start + _size - 1; start < end; ++start, --end){
			unsigned char swap = *start;
			*start = *end;
			*end = swap;		
		}	
	}
}

void date(int socket){
	int counter = 0;
	char time[24];
	char *temp;
	write(socket, "02", 2);
	counter = read(socket, time, 24);
	
	temp = time;	

	while(counter != 24){
		temp += strlen(time);
		read(socket, temp, (24 - counter));		
	}
	printf("Current date: %s\n", time);
}

void roots(double a, double b, double c, int socket){
	
	char rootsCounter;
	double x1, x2;
	write(socket, "01", 2);
	sleep(1);
	swapbytes(&a, sizeof(double));
	write(socket, &a, sizeof(a));
	sleep(1);
	swapbytes(&b, sizeof(double));
	write(socket, &b, sizeof(b));
	sleep(1);
	swapbytes(&c, sizeof(double));
	write(socket, &c, sizeof(c));
	sleep(1);
	read(socket, &rootsCounter, sizeof(char));
	
	if(atoi(&rootsCounter) == 0){
		printf("There's no roots.\n");
	} else if(atoi(&rootsCounter) == 1){
		printf("Result:\n");
		read(socket, &x1, sizeof(double));
		swapbytes(&x1, sizeof(double));
		printf("x =  %f\n", x1);
	} else if(atoi(&rootsCounter) == 2){
		printf("Result:\n");
		read(socket, &x1, sizeof(double));
		swapbytes(&x1, sizeof(double));
		printf("x1 = %f\n", x1);
		read(socket, &x2, sizeof(double));
		swapbytes(&x2, sizeof(double));
		printf("x2 = %f\n", x2);
	}
}