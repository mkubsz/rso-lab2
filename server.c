#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>

#define ROOTS 01
#define DATE 02

void date(int socket);
void roots(int socket);
bool isLittleEndian();
void swapbytes(double*_object, size_t _size);

int main(){ 

	int serverSocket, clientSocket;
	socklen_t ser_len, cli_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;;
	int orderId;	

	serverSocket = socket(AF_INET,SOCK_STREAM,0);
	
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	server_address.sin_port = htons(7500);
	ser_len = sizeof(server_address);
	
	bind(serverSocket, (struct sockaddr*) &server_address, ser_len);
	
	listen(serverSocket, 10);
	
	while(1){

		char response[10];
				
		printf("Waiting for client...\n");		

		cli_len = sizeof(client_address);
		clientSocket = accept(serverSocket, (struct sockaddr *) &client_address, &cli_len);
		
		read(clientSocket, &response, 2);
		
		orderId = atoi(response);	
		if(orderId == DATE)
			date(clientSocket);
		if(orderId == ROOTS)
			roots(clientSocket);
		
		sleep(1);
		close (clientSocket);
	}
}

bool isLittleEndian(){
	short int number = 0x1;
	char *numPtr = (char*)&number;
	return (numPtr[0] == 1);
}

void swapbytes(double*_object, size_t _size){
	unsigned char *start, *end;
	
	if(isLittleEndian()){
		for(start = (unsigned char *)_object, end = start + _size - 1; start < end; ++start, --end){
			unsigned char swap = *start;
			*start = *end;
			*end = swap;		
		}	
	}
}

void date(int socket){
		
	int counter;
	time_t rawtime;
	struct tm * timeinfo;
	char *temp;
	char curr_time[24];	

	time ( &rawtime );
	timeinfo = localtime( &rawtime );
	strcpy(curr_time, asctime(timeinfo));

	counter = write(socket, curr_time, 24);

	temp = curr_time;	

	while(counter != 24){
		temp += strlen(curr_time);
		write(socket, temp, (24 - counter));		
	}
}

void roots(int socket){
	double a, b, c, delta, x1,x2;
	read(socket, &a, sizeof(double));
	swapbytes(&a, sizeof(double));
	read(socket, &b, sizeof(double));
	swapbytes(&b, sizeof(double));
	read(socket, &c, sizeof(double));
	swapbytes(&c, sizeof(double));
	
	delta = ((b*b) - (4*a*c));
	if(delta < 0){
		write(socket, "0", sizeof(char));
	} else if(delta == 0){
		write(socket, "1", sizeof(char));
		x1 = (-1*b) / (2*a);
		swapbytes(&x1, sizeof(double));
		write(socket, &x1, sizeof(x1));
	} else{
		write(socket, "2", sizeof(char));
		x1 = (-1*b+sqrt(delta)) / (2*a);
		x2 = (-1*b-sqrt(delta)) / (2*a);
		swapbytes(&x1, sizeof(double));
		write(socket, &x1, sizeof(x1));
		swapbytes(&x2, sizeof(double));
		write(socket, &x2, sizeof(x2));
	}
}